<?php

$router->post('/user/create', 'UserController@create');

$router->post('/user/login', 'UserController@login');

$router->get('/recipe/list', 'RecipeController@getAll');

$router->post('/recipe/delete', ['middleware' => 'auth', 'uses' => 'RecipeController@delete']);

$router->post('/recipe/update', ['middleware' => 'auth', 'uses' => 'RecipeController@update']);

$router->post('/recipe/create', ['middleware' => 'auth', 'uses' => 'RecipeController@create']);
