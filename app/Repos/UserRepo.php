<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 09.08.2018
 * Time: 14:06
 */

namespace App\Repos;

use App\Models\User;

class UserRepo
{
    public static function findByToken($token){
        return User::query()
            ->where([['user_token', $token], ['token_expire','<=',  microtime(true)*10000]])
            ->first();
    }

    public static function addUser($name, $login, $password){
        $user = new User();
        $user->name = $name;
        $user->login = $login;
        $user->password = sha1($password);
        $user->save();
        return $user;
    }
}