<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 09.08.2018
 * Time: 12:15
 */

namespace App\Repos;


use App\Models\Recipes;
use App\Utils;
use Illuminate\Support\Facades\Auth;

class RecipeRepo
{
    public static function updateRecipeById($id, $title, $description, $image_url, $updated_by){
        $recipe = Recipes::query()->where('id', $id)->first();
        $recipe->title = $title;
        $recipe->description = $description;
        $recipe->image_url = $image_url;
        $recipe->updated_by = $updated_by;
        $recipe->save();
        return $recipe;
    }

    public static function deleteRecipeById($id){
        return Recipes::query()->where('id',$id)->delete();
    }

    public static function addRecipe($title, $description, $image_url, $author_id){
        $recipe = new Recipes();
        $recipe->title = $title;
        $recipe->description = $description;
        $recipe->image_url = $image_url;
        $recipe->author_id = $author_id;
        $recipe->save();
        return $recipe;
    }

    public static function getOneById($id){
        return Recipes::query()->where('id',$id)->first();
    }

    public static function getAll(){
        return Recipes::all();
    }
}