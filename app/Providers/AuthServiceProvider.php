<?php

namespace App\Providers;

use App\Repos\UserRepo;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['auth']->viaRequest('api', function ($request) {
            if ($request->input('api_token')) {
                $found = UserRepo::findByToken($request->input('api_token'));
                if ($found){
                    $request->request->add(['userId' => $found->id]);
                }
                return $found;
            }
        });
    }
}
