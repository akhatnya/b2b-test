<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 09.08.2018
 * Time: 11:39
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Recipes extends Model
{
    public $timestamps = false;
    protected $table = "recipes";
    protected $fillable = ['title','description','image_url', 'author_id', 'updated_by'];
}