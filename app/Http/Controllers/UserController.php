<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 08.08.2018
 * Time: 21:06
 */

namespace App\Http\Controllers;
use App\Models\User;
use App\Repos\UserRepo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class UserController extends Controller
{
    public function create(Request $request){
        return UserRepo::addUser(
            $request->name,
            $request->login,
            $request->password
        );
    }

    public static function findByToken($token){
        return UserRepo::findByToken($token);
    }

    public function login(Request $request){
        $user = User::query()
            ->where('login',$request->login)
            ->where('password',sha1($request->password));

        if ($user = $user->first()){

            $user->user_token = sha1(microtime(true) * 10000 + $user->id);
            $user->token_expire = microtime(true) * 10000 + 100000;
            $user->save();

            return ['api_token' => $user->user_token];

        }else{
            return new BadRequestHttpException();
        }
    }
}