<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 09.08.2018
 * Time: 14:55
 */

namespace App\Http\Controllers;
use App\Repos\RecipeRepo;
use App\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class RecipeController extends Controller
{
    public function create(Request $request)
    {
        if ($imageUrl = Utils::storeUploadedFile($_FILES['image'])) {
            return RecipeRepo::addRecipe($request->title, $request->description, $imageUrl, Auth::user()->id);
        } else {
            return new BadRequestHttpException();
        }
    }

    public function delete(Request $request){
        return RecipeRepo::deleteRecipeById($request->id);
    }

    public function update(Request $request){
        if ($imageUrl = Utils::storeUploadedFile($_FILES['image'])) {
            return RecipeRepo::updateRecipeById(
                $request->id,
                $request->title,
                $request->description,
                $imageUrl,
                Auth::user()->id
            );
        } else {
            return new BadRequestHttpException();
        }
    }

    public function getAll(Request $request){
        return RecipeRepo::getAll();
    }

    public function getOneById(Request $request){
        return RecipeRepo::getOneById($request->id);
    }
}