<?php
/**
 * Created by PhpStorm.
 * User: hp
 * Date: 09.08.2018
 * Time: 12:36
 */

namespace App;


use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class Utils
{
    public static function storeUploadedFile($file){
        $uploadPath = 'C:/xampp/htdocs/recipe/storage/app/public/';
        $fileName = md5(microtime(true) * 10000) . basename($file['name']);
        $filePath = $uploadPath . $fileName;

        if (move_uploaded_file($file['tmp_name'], $filePath)) {
            return $_SERVER['HTTP_HOST'] . "/recipe/storage/app/public/" . $fileName;
        } else {
            return new BadRequestHttpException();
        }
    }
}